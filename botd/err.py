# BOTD - IRC channel daemon.
#
# errors. 

class EOVERLOAD(Exception):

    pass

class EBLOCKING(Exception):

    pass

class ENOCLASS(Exception):

    pass

class ECLASS(Exception):

    pass

class EDEBUG(Exception):

    pass

class EEMPTY(Exception):

    pass

class EJSON(Exception):

    pass

class ENOFILE(Exception):

    pass

class ENOFUNCTION(Exception):

    pass

class ENOMODULE(Exception):

    pass

class ENOTFOUND(Exception):

    pass

class ENOTIMPLEMENTED(Exception):

    pass

class ENOTXT(Exception):

    pass

class ENOUSER(Exception):

    pass

class ENOTYPE(Exception):

    pass

class ETYPE(Exception):

    pass

class EOWNER(Exception):

    pass

class ERESERVED(Exception):

    pass

class ESET(Exception):

    pass

class EINIT(Exception):

    pass
